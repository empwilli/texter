#!/usr/bin/env python3.4

import re
import argparse
import sys
import random

follower   = {}
word_count = 0

parser = argparse.ArgumentParser(description="Write random texts, randomly!")
parser.add_argument('textbase', metavar='N', type=str, nargs='+',
                    help='The texts which are used as a basis for the generated text')
parser.add_argument('--outfile', type=str, help='The file the generated text should go to', default=sys.stdout)
parser.add_argument('--count', type=int, help='The number of words of the generated text, default is 300', default=300)
parser.add_argument('--seed', type=int, help='The seed which is used to generate the random text')

args = parser.parse_args()

for text in args.textbase:
    with open(text, 'r') as f:
        prev_token = '.'
        for line in f:
            word_count += len(line)
            tokens = re.findall(r"[\w']+|[.,!?;]", line)
            for i in tokens:
                if prev_token not in follower:
                    follower[prev_token] = {}
                if i not in follower[prev_token]:
                    follower[prev_token][i] = 0
                follower[prev_token][i] += 1
                prev_token = i

for f in follower.keys():
    count = 0
    word_list = []
    for word in follower[f].keys():
        count += follower[f][word]
        word_list.append((word, follower[f][word]))
    word_list = sorted(word_list, key=lambda tup: tup[1], reverse=True)
    word_list_new = [(x, y / count) for x, y in word_list]
    follower[f] = word_list_new

if not hasattr(args, 'seed'):
    r = random.seed()
else:
    r = random.seed(args.seed)

if args.outfile != sys.stdout:
    with open(args.outfile, 'w') as outfile:
        count = 0
        prev_word = '.'
        while count < args.count:
            num = random.uniform(0, 1)
            word = ''
            for f in follower[prev_word]:
                num -= f[1]
                word = f[0]
                if num <= 0:
                    break
            space = ""
            if word not in ".,;:!?" and count != 0:
                space = " "
            print("{}{}".format(space, word), file=outfile, end="")
            prev_word = word

        while prev_word not in ".!?":
            num = random.uniform(0, 1)
            word = ''
            for f in follower[prev_word]:
                num -= f[1]
                word = f[0]
                if num <= 0:
                    break

            space = ""
            if word not in ".,;:!?" and count != 0:
                space = " "
            print("{}{}".format(space, word), file=outfile, end="")
            prev_word = word
        print("")
else:
    prev_word = '.'
    for count in range(args.count):
        num = random.uniform(0, 1)
        word = ''
        for f in follower[prev_word]:
            num -= f[1]
            word = f[0]
            if num <= 0:
                break
        space = ""
        if word not in ".,;:!?" and count != 0:
            space = " "
        print("{}{}".format(space, word), file=args.outfile, end="")
        prev_word = word

    while prev_word not in ".!?":
        num = random.uniform(0, 1)
        word = ''
        for f in follower[prev_word]:
            num -= f[1]
            word = f[0]
            if num <= 0:
                break
        space = ""
        if word not in ".,;:!?" and count != 0:
            space = " "
        print("{}{}".format(space, word), file=args.outfile, end="")
        prev_word = word

    print("")

